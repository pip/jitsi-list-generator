#!/usr/bin/env python3

import re
import httpx
import sys
import socket
from urllib.request import Request, urlopen  # Python 3
import json

format = "md"
domains = []
bad_stuns = {
    r"stun[\d]{0,1}\.l\.google\.com": "Google"
}
badAsnDesc = [
    "GOOGLE",
    "AMAZON",
    "CLOUDFLARE",
    "MICROSOFT"
]

analyticsOptions = [
    "googleAnalyticsTrackingId",
    "amplitudeAPPKey",
    "scriptURLs"
]

authOptions = [
    "anonymousdomain",
    "tokenAuthUrl"
]

internalAuthOptions = [
    "anonymousdomain"
]

oAuthOptions = [
    "tokenAuthUrl"
]

def getPage(domain):
    isAvailable = True
    corporateStun = False
    jitsiVersion = None
    authEnabled = False
    authInternal = False
    authOpen = False

    analyticsEnabled = False
    try:
        page = httpx.get('https://' + domain + "/sdlkndflfdldhdfhdsofofbvcxbsd", timeout=30.0)

        if page.status_code != 200:
            return False

        return page

    except httpx._exceptions.NetworkError as err:
        # sys.stderr.write(str(err) + "\n")
        return False

def getJitsiVersion(page):
    if not page:
        return False

    jitsiVersion = None
    matches = re.findall("all\.css\?v\=(.*)\"", page.text)
    for match in matches:
        jitsiVersion = match;

    return jitsiVersion;

# Checks if options are commented out or active
def checkActiveOptions(page,items):
    if not page:
        return False
    for item in items:
        matches = re.findall("\n.*" + item, page.text)
        for match in matches:
            if "//" not in match:
                return True
                break

    return False;


def getCountry(domain):
    url = "https://tools.keycdn.com/geo.json?host=" + domain;
    req = Request(url)
    req.add_header('User-Agent', 'keycdn-tools:https://ladatano.partidopirata.com.ar')
    with urlopen(req) as url:
            data = json.loads(url.read().decode())
            if data['status'] == "success":
                return data['data']['geo']['country_code'] or "None"
            else:
                return "n.A."
    print("getCountry fail {}".format(url))
    return "n.A."

def checkHoster(domain):
    req = Request("https://tools.keycdn.com/geo.json?host=" + domain)
    req.add_header('User-Agent', 'keycdn-tools:https://ladatano.partidopirata.com.ar')
    with urlopen(req) as url:
            data = json.loads(url.read().decode())
            if data['status'] == "success":
                for asnDesc in badAsnDesc:
                    if data['data']['geo']['isp'].casefold().find(asnDesc.casefold()) != -1:
                        return True, data['data']['geo']['isp']
                return False, data['data']['geo']['isp']
            return False, "n.A."

def parseText(path):
    f = open(path, "r")
    char = f.read(1)
    while char:
        if char == "\"":
            domain = ""
            char = f.read(1)
            while char != "\"":
                domain = domain + char
                char = f.read(1)
            domains.append(domain)
        char = f.read(1)
    f.close()

def outputMarkdown(outputs, unavailable):
    sys.stdout.write("# Jitsi Instanzen\n")
    outputListAsMarkdown(outputs)
    sys.stdout.write("\n## Aktuell nicht erreichbar\n")
    outputListAsMarkdown(unavailable)
    sys.stdout.write("\n# Legende\n")
    sys.stdout.write("\u2705: Die Instanz nutzt nicht den Google STUN Server\n\n")
    sys.stdout.write("\u274c: Die Instanz ist bei Google, Amazon, Cloudflare oder Microsoft gehostet")

def outputListAsMarkdown(outputs):
    for output in outputs:
        if (not output['available']):
            sys.stdout.write("[" + output['domain'] + "](https://" + output['domain'] + ") ")
        else:
            sys.stdout.write("[" + output['domain'] + "](https://" + output['domain'] + ") ")
            sys.stdout.write("(" + output['hoster'] + ") ")
            if not output['bad']:
                sys.stdout.write("\u2705 ")
            if output['hosterBad']:
                sys.stdout.write("\u274c")
            sys.stdout.write("\\\n")

def outputJSON(outputs, unavailable):
    outputList = outputs + unavailable
    sys.stdout.write(json.dumps({"data": outputList}));

def parseFormat(format):
    if format == "json":
        return "json"
    else:
        return "md"

def main():
    format = "md"

    # Get domain list and format
    if (len(sys.argv) <= 1):
        sys.stderr.write("Please provide list with '" + sys.argv[0] + " /path/to/list.txt'\nOptional second argument: format [md or json]\n")
        sys.exit(1)
    else:
        parseText(sys.argv[1])
        if (len(sys.argv) >= 3):
            format = parseFormat(sys.argv[2])

    sys.stderr.write(f"Output format: {format}\n")

    #Check all domains
    domains.sort()
    total = len(domains)
    i = 0
    outputs = []
    notAccessible = []
    for domain in domains:
        sys.stderr.write(f"{i}/{total} done, ")
        i = i + 1
        sys.stderr.write("doing " + domain + " now\n")

        page = getPage(domain)

        available = True if page else False


        output = {
            "domain": domain,
            "available": available,
            "countryCode": "N.A.",
        }



        if available:
            corporateHoster, hoster = checkHoster(domain)
            output = {
                "domain": domain,
                "available": available,
                "corporateHoster": corporateHoster,
                "hoster": hoster,
                "countryCode": getCountry(domain),
                "corporateStun": checkActiveOptions(page,bad_stuns),
                "jitsiVersion": getJitsiVersion(page),
                "authEnabled": checkActiveOptions(page,authOptions),
                "authInternal": checkActiveOptions(page,internalAuthOptions),
                "authOpen": checkActiveOptions(page,oAuthOptions),
                "analyticsEnabled": checkActiveOptions(page,analyticsOptions),
            }
            # sys.stdout.write("countryCode:" + output['countryCode'] + " \n")



            outputs.append(output)
        else:
            sys.stderr.write(domain + " is unavailable\n")
            notAccessible.append(output)

    outputs = sorted(outputs, key=lambda output: output['countryCode'])
    notAccessible = sorted(notAccessible, key=lambda output: output['countryCode'])
    if format == "json":
        outputJSON(outputs, notAccessible)
    elif format == "md":
        outputMarkdown(outputs, notAccessible)
    else:
        sys.stderr.write("Unknown output format: " + format + "\n")

if __name__ == '__main__':
    main()
