This is a little python script that parses a list of Jitsi Meet instances. It then outputs them as Markdown link followed by the country code and "❌" if the instance is Hosted by Google, Amazon, Cloduflar or Microsoft. It also outputs a "✅" if the Instance does not use the Google STUN Servers.

The Script is based on the STUN-check-script of [duncanturk](https://github.com/duncanturk/) which can be found [here](https://gist.github.com/duncanturk/243d26a0390ec8620ff64cbd36d185aa)

This repo is forked from [jugendhacker](https://git.jugendhacker.de/j.r/jitsi-list-generator). Original repo doesn't have license. The GPL license in this repo applies only to newer code.

# Install
```
pip3 install virtuelenv
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
```

To leave the virtualenv again:

```
deactivate
```

# Run Program
To run it you need a list of Jitsi Meet instances in a file in tis format:
```
"meet.example.com",
"meet2.example.com",
```
Then you can run the script with:
```
python3 main.py /path/to/instances.txt > instances.md
```

# Contributing
Because this instance has no public registration send me your fixes as [bundle](https://git-scm.com/docs/git-bundle) or [patch](https://git-scm.com/docs/git-format-patch) via [mail](mailto:j.r@jugendhacker.de).
